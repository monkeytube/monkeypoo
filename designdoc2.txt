﻿TUBE MONKEY must explore the depth of this mysterious planet he just landed on. But the planet is inhabited by three SUPER SPACE ALIENS (SSA) which each have their own interest and powers. BOOZEY uses ALCOHOL to influence TUBE MONKEY, while LOOZEY uses DRUGS, and finally FLOOZEY who uses SEX APPEAL. Tube Monkey always moves forward but can turn left and right. The planet is littered with MONEY, ALCOHOL and DRUGS which TUBE MONKEY must selectively choose to collect or avoid. As TUBE MONKEY digs deeper, he will find even more ALCOHOL DRUGS AND MONEY, how much can he uncover in 5 minutes of drunken hallucinogenic sexy tubular exploration?!?! 

The three SUPER SPACE ALIENS

BOOZEY is the planet itself! He causes earthquakes and shakes and spins the world to disorient the player. BOOZEY uses his embedded accelerometer to control the natural rotation of the planet. Sometimes he gets so angry and excited that he shakes, and when that happens, the planet shakes as well. 

When TUBE MONKEY is under the influence of BOOZEY, he looses motor control. Sometimes the player may shift left, sometimes the player will shift left. When BAC reaches critical levels, TUBE MONKEY may make sudden turns or gain bursts of speed.

LOOZEY controls the underground wind currents of the planet. The gusts of wind traveling through the microtunnels (dug out by the micro moles centuries ago of course) affect the speed and orientation of TUBE MONKEY. It may cause the player to speed up or slow down and it may push TUBE MONKEY in a certain direction. LOOZEY, while powerful, is subject to the orientation of the planet, thus while LOOZEY can control the direction of the current, BOOZEY may change the direction by rotating the planet even before LOOZEY has a chance to compensate.

When TUBE MONKEY is under the affect of LOOZEY, he will have psychotic hallucinations. The screen image may twist and twirl and the colors may change, sometimes items start to look different, or even move, or perhaps he shall experience the dreaded pixel art filter. 

FLOOZY is a simple gal trying to make a simple living. When she sees a potential customer arrive on her barren home planet, she seizes this opportunity. FLOOZY has all the same powers as TUBE MONKEY and SEX APPEAL. She'll chase TUBE MONKEY down to the core of the planet making a quick buck every time she touches the player. 

When TUBE MONKEY is under the affect of FLOOZY, the screen will be littered with permanent lip shaped markings. How cute!
